# Queue Showcase

This project implements pub/sub messaging with queues. 

## How it works
![schema](doc/schema.png)

The producer puts a message to the _DEV.QUEUE.ALIAS_ queue which targets the _DEV.BASE.TOPIC_ topic.
From there a subscription forwards the message to the _DEV.QUEUE.1_ queue where the consumer can read the message. 
This method allows to convert point-to-point application to pub/sub.

## Build

Run `mvn clean package` to build the applications. You could reduce the build time by running the builds in parallel `mvn -T 2 clean package`

To create the container images you have to run `docker-compose build`. You can change the number of consumers by duplicating the consumer services inside the _docker-comspose.yml_
By running `docker-compose build --parallel` you can decrease the build time.

## IBM MQ Dashboard

The dashboard ca be reached here: _https://localhost:9443/ibmmq/console/login.html_

- username: admin
- password: passw0rd

## Usage

You can send a message by sending a GET request to: _http://localhost:8080/data/message?msg=YOUR_MESSAGE_HERE_

You should see your message in the container logs.

