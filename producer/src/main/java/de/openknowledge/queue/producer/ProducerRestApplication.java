package de.openknowledge.queue.producer;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 */
@ApplicationPath("/data")
public class ProducerRestApplication extends Application {
}
