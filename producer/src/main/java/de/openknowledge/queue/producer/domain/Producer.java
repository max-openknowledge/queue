package de.openknowledge.queue.producer.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.json.bind.JsonbBuilder;

@ApplicationScoped
public class Producer {

  private static final Logger LOG = LoggerFactory.getLogger(Producer.class);

  @Resource(lookup = "JMSFactory")
  private ConnectionFactory jmsFactory;

  @Resource(lookup = "JMSQueue")
  private Queue jmsQueue;

  public void send(String msg) {
    try (Connection connection = jmsFactory.createConnection();
        Session session = connection.createSession();
        MessageProducer producer = session.createProducer(jmsQueue)) {

      LOG.debug("Sending to queue: {}", jmsQueue.getQueueName());

      TextMessage textMessage = session.createTextMessage();
      String customMessage = createCustomMessage(msg);

      LOG.info("Sending message: {}", customMessage);

      textMessage.setText(customMessage);
      producer.send(textMessage);

    } catch (JMSException e) {
      LOG.error(e.getMessage(), e);
      e.printStackTrace();
    }
  }

  private String createCustomMessage(String msg) {
    return JsonbBuilder
        .create()
        .toJson(new CustomMessage(msg));
  }
}
